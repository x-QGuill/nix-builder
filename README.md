# Remote builder

Ask for the `BUILDER` address.

## Add your ssh key

```sh
ssh-copy-id -i ~/.ssh/id_rsa.pub BUILDER
```

The password is `nixos`.


## Copy the Nix Config

```
# ~/.config/nix/nix.conf or similar
experimental-features = nix-command flakes
builders = ssh://root@BUILDER
cores = 0
extra-sandbox-paths =
max-jobs = 0
require-sigs = false
sandbox = false
sandbox-fallback = false
substituters = http://BUILDER:8080 ssh-ng://root@BUILDER https://cache.nixos.org/
builders-use-substitutes = true
trusted-public-keys = BUILDER:snBDi/dGJICacgRUw4nauQ8KkSksAAAhCvPVr9OGTwk=
system-features = nixos-test benchmark big-parallel kvm
allowed-users = *
trusted-users = root
```
