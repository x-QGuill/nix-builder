{ pkgs, ... }:
let
  inherit (import ./ssh-keys.nix pkgs)
    snakeOilPrivateKey snakeOilPublicKey cachePub cachePriv;

  sshKeysConf = ''
    cp ${snakeOilPrivateKey} /root/.ssh/id_rsa
    chmod 600 /root/.ssh/id_rsa
    cp ${snakeOilPublicKey} /root/.ssh/id_rsa.pub
  '';

  commonConfig = { pkgs, ... }: {
    networking.firewall.enable = false;
    services.sshd.enable = true;
    services.openssh = {
      enable = true;
      ports = [ 22 ];
      permitRootLogin = "yes";
    };
    users.users.root.password = "nixos";
    environment.systemPackages = [ pkgs.git pkgs.htop ];
  };

in {
  nodes = {
    builder = { pkgs, ... }: {
      imports = [ commonConfig ];
      boot.postBootCommands = ''
        echo "${cachePriv}" > /root/cache-priv-key.pem
        echo "${cachePub}" > /root/cache-pub-key.pem
        echo "secret-key-files = /root/cache-priv-key.pem" >> /etc/nix/nix.conf
        nix sign-paths --all -k /root/cache-priv-key.pem
      '';
      services.nix-serve = {
        enable = true;
        port = 8080;
      };
      nix.extraOptions = ''
        experimental-features = nix-command flakes
      '';
    };
  };
  testScript = ''
    builder.succeed("true")
  '';
}
